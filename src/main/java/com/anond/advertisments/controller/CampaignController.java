package com.anond.advertisments.controller;

import com.anond.advertisments.dto.Campaign;
import com.anond.advertisments.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class CampaignController {

    @Autowired
    CampaignService campaignService;

    @RequestMapping(value = "/categories/{id}", method = GET)
    public ResponseEntity<Campaign> getCategory(@PathVariable Integer id) {
        Campaign campaign = campaignService.getCampaignById(id);
        if (campaign == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(campaign, HttpStatus.OK);
    }
}
