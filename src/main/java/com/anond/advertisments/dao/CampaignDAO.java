package com.anond.advertisments.dao;

import com.anond.advertisments.dto.Campaign;

import java.util.List;

public interface CampaignDAO {

    boolean insertCampaign(Campaign campaign);

    boolean updateCampaign(Campaign campaign);

    boolean deleteCampaign(Integer id);

    List<Campaign> getAllCampaigns();

    Campaign getCampaignById(Integer id);

}
