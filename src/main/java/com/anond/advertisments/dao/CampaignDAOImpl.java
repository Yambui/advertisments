package com.anond.advertisments.dao;

import com.anond.advertisments.dbutils.JDBCConnectionUtil;
import com.anond.advertisments.dto.Campaign;
import com.anond.advertisments.parsers.CampaignParser;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class CampaignDAOImpl implements CampaignDAO{

    @Override
    public boolean insertCampaign(Campaign campaign) {
        Connection connection = JDBCConnectionUtil.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO campaign VALUES (NULL, ?,?,?,?)");
            preparedStatement.setString(1, campaign.getName());
            preparedStatement.setInt(2, campaign.getStatus());
            preparedStatement.setDate(3, new Date(campaign.getStartDate().getTime()));
            preparedStatement.setDate(4, new Date(campaign.getEndDate().getTime()));
            int result = preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
            if (result == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public Campaign getCampaignById(Integer id){
        Connection connection = JDBCConnectionUtil.getConnection();
        Campaign campaign = null;
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM campaign WHERE id=" + id);
                if (resultSet.next()) {
                    campaign = CampaignParser.parseCampaign(resultSet);
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return campaign;
    }

    @Override
    public boolean deleteCampaign(Integer id){
        int result = JDBCConnectionUtil.executeUpdateOnDb("DELETE FROM campaign WHERE id=" + id);
        if (result == 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean updateCampaign(Campaign campaign){
        Connection connection = JDBCConnectionUtil.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE campaign SET NAME=?, STATUS=?,STARTDATE=?,ENDDATE=? WHERE ID=?");
            preparedStatement.setString(1, campaign.getName());
            preparedStatement.setInt(2, campaign.getStatus());
            preparedStatement.setDate(3, new Date(campaign.getStartDate().getTime()));
            preparedStatement.setDate(4, new Date(campaign.getEndDate().getTime()));
            preparedStatement.setInt(5, campaign.getId());
            int result = preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
            if (result == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Campaign> getAllCampaigns() {
        Connection connection = JDBCConnectionUtil.getConnection();
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign;
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM campaign");
                while (resultSet.next()) {
                    campaign = CampaignParser.parseCampaign(resultSet);
                    campaigns.add(campaign);
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return campaigns;
    }

}
