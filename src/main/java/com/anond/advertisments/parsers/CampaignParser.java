package com.anond.advertisments.parsers;

import com.anond.advertisments.dto.Campaign;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class CampaignParser {

    public static Campaign parseCampaign(ResultSet resultSet) throws SQLException {
        Campaign campaign = new Campaign();
        campaign.setId(resultSet.getInt("ID"));
        campaign.setName(resultSet.getString("NAME"));
        campaign.setStatus(resultSet.getInt("STATUS"));
        campaign.setStartDate(new Date(resultSet.getDate("STARTDATE").getTime()));
        campaign.setEndDate(new Date(resultSet.getDate("ENDDATE").getTime()));
        return campaign;
    }
}
