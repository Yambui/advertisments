package com.anond.advertisments.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Campaign {

    private Integer id;
    private String name;
    private Integer status;
    private Date startDate;
    private Date endDate;

    public Campaign() {
    }

    public Campaign(Integer id, String name, Integer status, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
