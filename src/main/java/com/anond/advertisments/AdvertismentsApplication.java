package com.anond.advertisments;

import com.anond.advertisments.dao.CampaignDAO;
import com.anond.advertisments.dao.CampaignDAOImpl;
import com.anond.advertisments.dbutils.JDBCConnectionUtil;
import com.anond.advertisments.dto.Campaign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class AdvertismentsApplication {

	@Autowired
	CampaignDAO campaignDAO;

	public static void main(String[] args) {
		SpringApplication.run(AdvertismentsApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		System.out.println("Hi there from boot");
		JDBCConnectionUtil.initializeDb();
		Campaign campaign = new Campaign(null,"NAME", 5,new Date(), new Date());
		campaignDAO.insertCampaign(campaign);
		Campaign campaign1 = campaignDAO.getCampaignById(1);
		System.out.println(campaign1);
	}

}
