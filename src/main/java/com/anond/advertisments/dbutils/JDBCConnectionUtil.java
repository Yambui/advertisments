package com.anond.advertisments.dbutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCConnectionUtil {
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:file:~/test";

    static final String USER = "sa";
    static final String PASS = "sa";

    static final String INITIALIZATION_SCRIPT = "CREATE TABLE CAMPAIGN ("
            + "ID INT AUTO_INCREMENT PRIMARY KEY,"
            + "NAME VARCHAR(255),"
            + "STATUS INT,"
            + "STARTDATE DATE,"
            + "ENDDATE DATE);";

    static final String CLEANUP_SCRIPT = "DROP TABLE IF EXISTS CAMPAIGN;";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return connection;
    }

    public static int executeUpdateOnDb(String sql) {
        Connection connection = getConnection();
        int result = 0;
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                result = statement.executeUpdate(sql);
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void initializeDb() {
        executeUpdateOnDb(CLEANUP_SCRIPT);
        executeUpdateOnDb(INITIALIZATION_SCRIPT);
    }

}
