package com.anond.advertisments.service;

import com.anond.advertisments.dao.CampaignDAO;
import com.anond.advertisments.dto.Campaign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CampaignServiceImpl implements CampaignService{

    @Autowired
    CampaignDAO campaignDAO;

    @Override
    public boolean insertCampaign(Campaign campaign) {
        return campaignDAO.insertCampaign(campaign);
    }

    @Override
    public Campaign getCampaignById(Integer id) {
        return campaignDAO.getCampaignById(id);
    }
}
