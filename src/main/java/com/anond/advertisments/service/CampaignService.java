package com.anond.advertisments.service;

import com.anond.advertisments.dto.Campaign;

public interface CampaignService {

    boolean insertCampaign(Campaign campaign);

    Campaign getCampaignById(Integer id);
}
